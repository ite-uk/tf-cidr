# README #

Terraform script to extract the Atlassian IPv4 addresses

* Version 1.0
* Author: T. De Vos
* Date: 02/07/2020

### How do I run this TF script? ###

* terraform init
* terraform apply
