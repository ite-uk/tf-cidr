/*
 * Author. T. De Vos
 * Date: 2nd of July 2020
*/

terraform {
  required_version = ">= 0.12.6"
}

provider "http" {
}

##############################################
# - Declare an HTTP data source (use HTTPS endpoint) 
# - Perform a GET to the URL 
# - Set JSON headers
# - returns "atlassian-ip-ranges" containing the raw body
##############################################
data "http" "atlassian-ip-ranges" {
  url = "https://ip-ranges.atlassian.com/"

  # Define request headers
  request_headers = {
    Accept = "application/json"
  }
}

##############################################
# Variables
##############################################
locals {
  # Using the "body" of JSON String, obtain just the CIDR element 
  # Filter out any non-IPv4 IP addresses 
  # IP4v - 15 characters plus max 3 for the subnet mask    
  all_ipv4_addresses = [for r in jsondecode(data.http.atlassian-ip-ranges.body).items: r.cidr if length(r.cidr) <= 18] 
}

##############################################
# Output
##############################################
output "ip_ranges_ipv4" {
   description = "The list of IPv4 addresses from Atlassian"
   value = local.all_ipv4_addresses
}

